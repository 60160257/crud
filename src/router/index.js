import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import Form from '../views/Form.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/form',
    name: 'form',
    component: () => import('../views/Form.vue')
    // component: Form
  },
  {
    path: '/showview',
    name: 'showview',
    component: () => import('../views/ShowView.vue')
    // component: Form
  },
  {
    path: '/table',
    name: 'table',
    component: () => import('../views/Table.vue')
    // component: Form
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/counter',
    name: 'counter',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Counter.vue')
  },
  {
    path: '/users',
    name: 'users',
    component: () => import('../views/Users')
    // component: Form
  },
  {
    path: '/indexform',
    name: 'indexform',
    component: () => import('../views/Users/indexform.vue')
    // component: Form
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
